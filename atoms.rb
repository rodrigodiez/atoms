class Atom

  def initialize(neutrons_num=nil, electrons_num=nil)

    raise 'One not simply instantiates an Atom...' if not defined?(@protons_num)

    @neutrons_num = (neutrons_num || @protons_num).to_i
    @electrons_num = (electrons_num || @protons_num).to_i
    @orbitals = [
      SOrbital.new(1),
      SOrbital.new(2), POrbital.new(2),
      SOrbital.new(3), POrbital.new(3), DOrbital.new(3),
      SOrbital.new(4), POrbital.new(4), DOrbital.new(4), FOrbital.new(4),
      SOrbital.new(5), POrbital.new(5), DOrbital.new(5), FOrbital.new(5),
      SOrbital.new(6), POrbital.new(6), DOrbital.new(6),
      SOrbital.new(7), POrbital.new(7),
      SOrbital.new(8)
    ]

    @electrons_num.times do
      gain_electron
    end
  end

  def gain_electron

    @orbitals.each do |orbital|

      begin
        orbital.gain_electron
        break
      rescue
      end
    end
  end

  def loose_electron

    @orbitals.reverse_each do |orbital|

      begin
        orbital.loose_electron
        break
      end
    end
  end

  def electron_configuration

    array_configuration = []

    @orbitals.each do |orbital|
      array_configuration.push([orbital.main_num.to_s, orbital.azimuth, orbital.electron_count.to_s].join)
    end

    array_configuration.join(',')
  end

end

class Hydrogen < Atom

  def initialize(neutrons_num=nil, electrons_num=nil)

    @protons_num = 1

    super(neutrons_num, electrons_num)
  end
end

class Helium < Atom

  def initialize(neutrons_num=nil, electrons_num=nil)

    @protons_num = 2

    super(neutrons_num, electrons_num)
  end
end

class Lithium < Atom

  def initialize(neutrons_num=nil, electrons_num=nil)

    @protons_num = 3

    super(neutrons_num, electrons_num)
  end
end

class Orbital
  attr_reader :main_num, :azimuth, :electron_count

  def initialize(main_num)

    raise 'One not simply instantiates an Orbital' if not defined?(@azimuth) or not defined?(@capacity)
    raise 'One not simply create an orbital higher than 8' if main_num > 8
    raise 'One not simply create an orbital lower than 1' if main_num < 1

    @main_num = main_num.to_i
    @electron_count = 0
  end

  def gain_electron
    if @electron_count.next > @capacity

      raise "One not simply puts #{@electron_count.next} electrons within a #{@capacity} electrons capacity orbital"
    end

    @electron_count += 1
  end

  def loose_electron
    if @electron_count.pred < 0

      raise "One not simply puts #{@electron_count.pred} electrons within an orbital"
    end

    @electron_count += 1
  end
end

class SOrbital < Orbital

  def initialize(main_num)

    main_num = main_num.to_i
    @azimuth = 's'
    @capacity = 2

    super(main_num)
  end
end

class POrbital < Orbital

  def initialize(main_num)

    main_num = main_num.to_i

    raise 'One not simply create a p orbital higher than 7' if main_num > 7
    raise 'One not simply create a p orbital lower than 2' if main_num < 2

    @azimuth = 'p'
    @capacity = 6

    super(main_num)
  end
end

class DOrbital < Orbital

  def initialize(main_num)

    main_num = main_num.to_i

    raise 'One not simply create a d orbital higher than 6' if main_num > 6
    raise 'One not simply create a d orbital lower than 3' if main_num < 3

    @azimuth = 'd'
    @capacity = 10

    super(main_num)
  end
end

class FOrbital < Orbital

  def initialize(main_num)

    main_num = main_num.to_i

    raise 'One not simply create a f orbital higher than 5' if main_num > 5
    raise 'One not simply create a f orbital lower than 4' if main_num < 4

    @azimuth = 'f'
    @capacity = 14

    super(main_num)
  end
end
